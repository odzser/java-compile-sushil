import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.util.logging.Logger;

import java.net.URI;

public class HadoopWriter {


    private FSDataOutputStream outputStream;
    private Logger logger;

    HadoopWriter(Configuration conf, String hdfsUri ,String fileName){

        logger = Logger.getLogger("MAIN");

        try {

            // ====== Init HDFS File System Object

//            System.setProperty("hadoop.home.dir", "");
//Get the filesystem - HDFS
            FileSystem fs = FileSystem.get(URI.create(hdfsUri), conf);

            logger.info("Begin Write file into hdfs");
//Create a path
            Path hdfswritepath = new Path( fileName);
//Init output stream
            outputStream = fs.create(hdfswritepath);
//Cassical output stream usage


        }
        catch (Exception e){
            logger.info(e.getMessage());
        }
    }


    public void writeData(String s) throws Exception{
        outputStream.writeBytes(s);
        outputStream.flush();
    }


    public  void  closeFile() throws Exception{
        outputStream.close();
        logger.info("End Write file into hdfs");

    }
}

