import org.apache.hadoop.conf.Configuration;

public class Main {

    public static void main(String[] args){
        Configuration conf = new Configuration();

        String hdfsuri = "hdfs://52.66.199.82:8020/";
// Set FileSystem URI
        conf.set("fs.defaultFS", hdfsuri);
// Because of Maven
//            conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
//            conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
// Set HADOOP user
        System.setProperty("HADOOP_USER_NAME", "Insight");

        HadoopWriter hadoopWriter = new HadoopWriter(conf,hdfsuri, Long.toString(System.currentTimeMillis()));
        Long startTime = System.currentTimeMillis();
        try {
            for (Integer i = 0; i < 100000000; i++) {
                hadoopWriter.writeData(i.toString());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Long stopTime = System.currentTimeMillis();
        System.out.println((stopTime-startTime)/1000);

        try {
            hadoopWriter.closeFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
